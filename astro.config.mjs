import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: 'https://ried.gir3.eu/',
  outDir: 'public',
  publicDir: 'static'
});
